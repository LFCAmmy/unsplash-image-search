# UnSplash Image Search

A simple image searching app.
Supported for ios 9.0 or above

## Table of Contents

- [Feature](#feature)
- [Install](#install)
- [Test Execution Step](#test-execution-step)
- [Maintainers](#maintainers)
- [License](#license)

## Feature

1. Program starts, lists default pictures related star wars
2. Users can search their preferred image through search bar
3. Users can filter the search result by Sort by, color and orientation

## Install

This project uses [CocoaPods](https://cocoapods.org/). Go check them out if you don't have them locally installed. If you already have CocoaPods installed, run the following command to install the pod

```sh
$ pod install
```

## Test Execution Step


Press the following keyboard keys(for mac) to run the test in XCode:
- ⌘U 

or

1) Press the test navigator in the left top menu
2) Click on run button on the icon with the project name from the left navitaion


## Maintainers

[@LFCAmmy](https://github.com/LFCAmmy).

## License

[MIT](LICENSE) © LFCAmmy


