//
//  LandingScreenInteractorTest.swift
//  Unsplash Image SearchTests
//
//  Created by Armaan Shrestha on 10/09/2021.
//

import XCTest
@testable import Unsplash_Image_Search

class LandingScreenInteractorTest: XCTestCase {
    
    var view: LandingScreenModuleInterface?
    var interactor: LandingScreenInteractor?
    var presenter = LandingScreenMockPresenter()
    var service = LandingScreenMockNetwork()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        interactor = LandingScreenInteractor(service: service)
        interactor?.output = presenter
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testFetchAllUnsplashPhotosSuccess() {
        service.currentError = nil
        interactor?.getCategoryPhotos()
    }
    
    func testfetchAllUpsplashPhotosFaliure() {
        service.currentError = "Error"
        presenter.currentError = "Error"
        interactor?.getCategoryPhotos()
    }
    
    func testFetchPaginatedUnsplashPhotosSuccess() {
        service.currentError = nil
        presenter.currentError = nil
        interactor?.getMorePhotos(pageNo: 1)
    }
    
    func testfetchPaginatedUpsplashPhotosFaliure() {
        service.currentError = "Error"
        presenter.currentError = "Error"
        interactor?.getMorePhotos(pageNo: 1)
    }
    
//    func testFetchSearchPhotoSuccess() {
//        service.currentError = nil
//        presenter.currentError = nil
//        interactor?.getSearchedPhotos()
//        print("asdasd", presenter.isSearchedPhotoCalled)
//        if !presenter.isSearchedPhotoCalled {
//            XCTFail("SearchPhoto func not called")
//        }
//    }
    
    
    func testFetchSearchPhotoFailure() {
        service.currentError = "Error"
        presenter.currentError = "Error"
        interactor?.getSearchedPhotos()
    }
    
//    func testFetchSearchPhoto() {
//        service.currentError = nil
//        presenter.currentError = nil
//        interactor?.getSearchedPhotos()
//        if !presenter.isSearchedPhotoCalled {
//            XCTFail("SearchPhoto func not called")
//        }
//    }


}

class LandingScreenMockPresenter: LandingScreenInteractorOutput {
    
    var expectation: XCTestExpectation?
    
    var currentError: String?
    
    func obtained(model: [UnSplashPhotoStructure]) {
        let staticList = Response.photosList
        XCTAssertEqual(staticList.count, model.count)
    }
    
    func obtained(paginationModel: [UnSplashPhotoStructure]) {
        let staticList = Response.photosList
        XCTAssertEqual(staticList.count, paginationModel.count)
    }
    
    var isSearchedPhotoCalled = false
    func searchedPhoto() {
        isSearchedPhotoCalled = true
    }
    
    func obtained(error: String) {
        XCTAssertEqual(currentError, error)
    }
    
}

class LandingScreenMockNetwork: LandingScreenServiceType {
    
    var currentError: String?
    
    func fetchCollectionPhotos(page: Int, success: @escaping complitionSuccess<[UnSplashPhoto]>, failed: @escaping complitionFailed) {
        if let currentError = currentError {
            failed(currentError)
        } else {
            success(Response.photosList)
        }
    }
    
    func searchPhotos(query: String, order_by: String, color: String, orientation: String, page: Int, success: @escaping complitionSuccess<SearchPhoto>, failed: @escaping complitionFailed) {
        if let currentError = currentError {
            failed(currentError)
        } else {
            print(Response.searchedPhotoList)
            success(Response.searchedPhotoList)
        }
    }
    
}
