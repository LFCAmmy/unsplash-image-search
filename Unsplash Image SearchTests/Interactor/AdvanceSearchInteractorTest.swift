//
//  AdvanceSearchInteractorTest.swift
//  Unsplash Image SearchTests
//
//  Created by Armaan Shrestha on 11/09/2021.
//

import XCTest
@testable import Unsplash_Image_Search

class AdvanceSearchInteractorTest: XCTestCase {
    
    var view: AdvanceSearchModuleInterface?
    var interactor: AdvanceSearchInteractor?
    var presenter = AdvanceSearchMockPresenter()
    var service = AdvanceSearchMockNetwork()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        interactor = AdvanceSearchInteractor(service: service)
        interactor?.output = presenter
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testFetchAdfanceSearchSuccess() {
        service.currentError = nil
        presenter.currentError = nil
        interactor?.advanceSearch()
        if !presenter.isSearchedPhotoCalled {
            XCTFail("SearchPhoto func not called")
        }
    }
    
    func testFetchAdfanceSearchFailure() {
        service.currentError = "Error"
        presenter.currentError = "Error"
        interactor?.advanceSearch()
    }

}

class AdvanceSearchMockPresenter: AdvanceSearchInteractorOutput {
    
    var expectation: XCTestExpectation?
    
    var currentError: String?
    
    var isSearchedPhotoCalled = false
    func searchedPhoto() {
        isSearchedPhotoCalled = true
    }
    
    func obtained(error: String) {
        XCTAssertEqual(currentError, error)
    }
}

class AdvanceSearchMockNetwork: AdvanceSearchServiceType {
    
    var currentError: String?
    
    func searchPhotos(query: String, order_by: String, color: String, orientation: String, page: Int, success: @escaping complitionSuccess<SearchPhoto>, failed: @escaping complitionFailed) {
        print("M here")
        if let currentError = currentError {
            failed(currentError)
        } else {
            success(Response.searchedPhotoList)
        }
    }
    
}
