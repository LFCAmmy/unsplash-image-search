//
//  SearchedPhotoInteractorTest.swift
//  Unsplash Image SearchTests
//
//  Created by Armaan Shrestha on 11/09/2021.
//

import XCTest
@testable import Unsplash_Image_Search

class SearchedPhotoInteractorTest: XCTestCase {

    var view: SearchedPhotoModuleInterface?
    var interactor: SearchedPhotoInteractor?
    var presenter = SearchedPhotoMockPresenter()
    var service = SearchedPhotoMockNetwork()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        interactor = SearchedPhotoInteractor(service: service)
        interactor?.output = presenter
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testFetchSearchPhoto() {
        interactor?.searchedPhoto = Response.searchedPhotoList
        interactor?.getSearchPhoto()
    }
    
    func testFetchPaginatedSearchPhotoSuccess() {
        service.currentError = nil
        presenter.currentError = nil
        interactor?.showMorePhotos(pageNo: 1)
    }
    
    func testFetchPaginatedSearchPhotoFailure() {
        service.currentError = "Error"
        presenter.currentError = "Error"
        interactor?.showMorePhotos(pageNo: 1)
    }
    
    func testFetchFilteredSearchPhoto() {
        interactor?.filteredSearchedPhoto = Response.searchedPhotoList
        interactor?.getFilteredSearchPhoto()
    }

}

class SearchedPhotoMockPresenter: SearchedPhotoInteractorOutput {
    
    var expectation: XCTestExpectation?
    
    var currentError: String?
    
    func obtained(data: [UnSplashPhotoStructure], totalPage: Int) {
        let staticList = Response.searchedPhotoList
        XCTAssertEqual(staticList.results.count, data.count)
        XCTAssertEqual(staticList.total_pages, totalPage)
    }
    
    func obtained(moreData: [UnSplashPhotoStructure], totalPage: Int) {
        let staticList = Response.searchedPhotoList
        XCTAssertEqual(staticList.results.count, moreData.count)
        XCTAssertEqual(staticList.total_pages, totalPage)
    }
    
    func obtained(filteredData: [UnSplashPhotoStructure], totalPage: Int) {
        let staticList = Response.searchedPhotoList
        XCTAssertEqual(staticList.results.count, filteredData.count)
        XCTAssertEqual(staticList.total_pages, totalPage)
    }
    
    func obtained(error: String) {
        XCTAssertEqual(currentError, error)
    }
}

class SearchedPhotoMockNetwork: SearchedPhotoServiceType {
    
    var currentError: String?
    
    func searchPhotos(query: String, order_by: String, color: String, orientation: String, page: Int, success: @escaping complitionSuccess<SearchPhoto>, failed: @escaping complitionFailed) {
        if let currentError = currentError {
            failed(currentError)
        } else {
            success(Response.searchedPhotoList)
        }
    }
    
}
