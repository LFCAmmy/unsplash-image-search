//
//  MockResponse.swift
//  Unsplash Image SearchTests
//
//  Created by Armaan Shrestha on 11/09/2021.
//

import Foundation
@testable import Unsplash_Image_Search

struct Response {
    static let photosList = [UnSplashPhoto(id: "1as5d4", width: 1564, height: 657, color: "#1548aa", urls: URLKind(raw: "mock", full: "mock", regular:                         "mock", small: "mock", thumb: "mock")),
                             UnSplashPhoto(id: "sad21s", width: 875, height: 487, color: "#12da1e", urls: URLKind(raw: "mock1", full: "mock2", regular: "mock3", small: "mock4", thumb: "mock5"))]
    
    static let searchedPhotoList = SearchPhoto(total: 150, total_pages: 7, results: photosList)
}
