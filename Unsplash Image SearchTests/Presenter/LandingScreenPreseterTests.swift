//
//  LandingScreenPreseterTests.swift
//  Unsplash Image SearchTests
//
//  Created by Armaan Shrestha on 10/09/2021.
//

import XCTest
@testable import Unsplash_Image_Search

class LandingScreenPreseterTests: XCTestCase {
    
    var presenter: LandingScreenPresenter!
    var view = LandingScreenMockView()
    var interactor = LandingScreenInteractorMock()
    var wireframe = LandingScreenWireframeMock()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        presenter = LandingScreenPresenter()
        presenter.view = view
        presenter.interactor = interactor
        interactor.presenter = presenter
        presenter.wireframe = wireframe
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFetchUnSplashPhotos() {
        presenter.viewIsReady()
        if !view.isLoaded {
            XCTFail("showLoading func not called")
        }
        if !view.isStoped {
            XCTFail("hideLoading func not called")
        }
        if !view.isSaveFetchUnSplashPhotoCalled {
            XCTFail("saveFetchUnSplashPhoto func not called")
        }
    }
    
    func testFetchPaginationData() {
        presenter.showMorePhotos(pageNo: 1)
        if !view.isLoaded {
            XCTFail("showLoading func not called")
        }
        if !view.isStoped {
            XCTFail("hideLoading func not called")
        }
        if !view.isSaveFetchUnSplashPhotoCalled {
            XCTFail("fetchPaginationUnSplashPhoto func not called")
        }
    }
    
    func testFetchSearchedPhoto() {
        presenter.searchPhoto()
        if !wireframe.isSearchPhotoCalled {
            XCTFail("SearchPhoto func not called")
        }
    }
    
    func testErrorMessages() {
        let error = "Server Error"
        presenter.obtained(error: error)
        XCTAssertEqual(view.error, error)
    }

}

class LandingScreenMockView: LandingScreenViewInterface {
    var isLoaded = false
    func showLoading() {
        isLoaded = true
    }
    
    var isStoped = false
    func hideLoading() {
        isStoped = true
    }
    
    var isSaveFetchUnSplashPhotoCalled = false
    func show(data: [UnSplashPhotoViewModel]) {
        isSaveFetchUnSplashPhotoCalled = true
    }
    
    var isPaginationDataCalled = false
    func show(paginationData: [UnSplashPhotoViewModel]) {
        isPaginationDataCalled = true
    }
    
    var error = String()
    func show(error: String) {
        self.error = error
    }
    
}

class LandingScreenInteractorMock: LandingScreenInteractorInput {
    var presenter : LandingScreenInteractorOutput?
    func getCategoryPhotos() {
        let photos = UnSplashPhotoStructure(id: "1as5d4", width: 1564, height: 657, color: "#1548aa", urls: URLKind(raw: "mock", full: "mock", regular: "mock", small: "mock", thumb: "mock"))
        presenter?.obtained(model: [photos])
    }
    
    func getMorePhotos(pageNo: Int) {
        let photos = UnSplashPhotoStructure(id: "1as5d4", width: 1564, height: 657, color: "#1548aa", urls: URLKind(raw: "mock", full: "mock", regular: "mock", small: "mock", thumb: "mock"))
        presenter?.obtained(model: [photos])
    }
    
    func getSearchedPhotos() {
        presenter?.searchedPhoto()
    }
}

class LandingScreenWireframeMock: LandingScreenWireframeInput {
    var view: UIViewController!
    
    var storyboardName: String = ""
    
    func getMainView() -> UIViewController {
        return view
    }
    
    var isSearchPhotoCalled = false
    func goToSearchPhoto() {
        isSearchPhotoCalled = true
    }
}

