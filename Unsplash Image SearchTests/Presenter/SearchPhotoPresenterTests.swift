//
//  SearchPhotoPresenterTests.swift
//  Unsplash Image SearchTests
//
//  Created by Armaan Shrestha on 10/09/2021.
//

import XCTest
@testable import Unsplash_Image_Search

class SearchPhotoPresenterTests: XCTestCase {
    
    var presenter: SearchedPhotoPresenter!
    var view = SearchPhotoMockView()
    var interactor = SearchPhotoInteractorMock()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        presenter = SearchedPhotoPresenter()
        presenter.view = view
        presenter.interactor = interactor
        interactor.presenter = presenter
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFetchSearchedPhoto() {
        interactor.getSearchPhoto()
        if !view.isFetchSearchPhotoCalled {
            XCTFail("fetchSearchPhoto func not called")
        }
    }
    
    func testFetchPaginationSearchedPhoto() {
        presenter.showMorePhotos(pageNo: 1)
        if !view.isLoading {
            XCTFail("showLoading func not called")
        }
        if !view.isStopLoading {
            XCTFail("hideLoading func not called")
        }
        if !view.isFetchPaginationSearchPhotoCalled {
            XCTFail("fetchPaginationSearchedPhoto func not called")
        }
    }
    
    func testFetchFilteredSearchPhoto() {
        interactor.getFilteredSearchPhoto()
        if !view.isFetchFilteredSearchPhotoCalled {
            XCTFail("fetchFilteredSearchPhoto func not called")
        }
    }

    func testErrorMessages() {
        let error = "Server Error"
        presenter.obtained(error: error)
        XCTAssertEqual(view.error, error)
    }
}

class SearchPhotoMockView: SearchedPhotoViewInterface {
    var isLoading = false
    func showLoading() {
        isLoading = true
    }
    
    var isStopLoading = false
    func hideLoading() {
        isStopLoading = true
    }
    
    var isFetchSearchPhotoCalled = false
    func show(data: [UnSplashPhotoViewModel], totalPage: Int) {
        isFetchSearchPhotoCalled = true
    }
    
    var isFetchPaginationSearchPhotoCalled = false
    func show(moreData: [UnSplashPhotoViewModel], totalPage: Int) {
        isFetchPaginationSearchPhotoCalled = true
    }
    
    var isFetchFilteredSearchPhotoCalled = false
    func show(filteredData: [UnSplashPhotoViewModel], totalPage: Int) {
        isFetchFilteredSearchPhotoCalled = true
    }
    
    var error = String()
    func show(error: String) {
        self.error = error
    }

}

class SearchPhotoInteractorMock: SearchedPhotoInteractorInput {
    var presenter: SearchedPhotoInteractorOutput?
    
    func getSearchPhoto() {
        let photos = UnSplashPhotoStructure(id: "1as5d4", width: 1564, height: 657, color: "#1548aa", urls: URLKind(raw: "mock", full: "mock", regular: "mock", small: "mock", thumb: "mock"))
        presenter?.obtained(data: [photos], totalPage: 5)
    }
    
    func showMorePhotos(pageNo: Int) {
        let photos = UnSplashPhotoStructure(id: "1as5d4", width: 1564, height: 657, color: "#1548aa", urls: URLKind(raw: "mock", full: "mock", regular: "mock", small: "mock", thumb: "mock"))
        presenter?.obtained(moreData: [photos], totalPage: 5)
    }
    
    func getFilteredSearchPhoto() {
        let photos = UnSplashPhotoStructure(id: "1as5d4", width: 1564, height: 657, color: "#1548aa", urls: URLKind(raw: "mock", full: "mock", regular: "mock", small: "mock", thumb: "mock"))
        presenter?.obtained(filteredData: [photos], totalPage: 7)
    }
    
}

