//
//  AdvanceSearchPresenterTests.swift
//  Unsplash Image SearchTests
//
//  Created by Armaan Shrestha on 10/09/2021.
//

import XCTest
@testable import Unsplash_Image_Search

class AdvanceSearchPresenterTests: XCTestCase {
    
    var presenter: AdvanceSearchPresenter!
    var view = AdvanceSearchMockView()
    var interactor = AdvanceSearchInteractorMock()
    var wireframe = AdvanceSearchWireframeMock()


    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        presenter = AdvanceSearchPresenter()
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        interactor.presenter = presenter
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testFetchAdvanceSearch() {
        presenter.advanceSearch()
        if !view.isLoading {
            XCTFail("showLoading func not called")
        }
        if !view.isStopLoading {
            XCTFail("hideLoading func not called")
        }
        if !interactor.isAsd {
            XCTFail("goToSearchPhoto func not called")
        }
        if !wireframe.isSearchPhotoCalled {
            XCTFail("goToSearchPhoto func not called")
        }
    }

}

class AdvanceSearchMockView: AdvanceSearchViewInterface {
    var isLoading = false
    func showLoading() {
        isLoading = true
    }
    
    var isStopLoading = false
    func hideLoading() {
        isStopLoading = true
    }
    
    var error = String()
    func show(error: String) {
        self.error = error
    }

}

class AdvanceSearchInteractorMock: AdvanceSearchInteractorInput {
    var presenter: AdvanceSearchInteractorOutput?
    
    var isAsd = false
    func advanceSearch() {
        isAsd = true
        presenter?.searchedPhoto()
    }

}

class AdvanceSearchWireframeMock: AdvanceSearchWireframeInput {
    
    var view: UIViewController!
    
    var storyboardName: String = ""
    
    func getMainView() -> UIViewController {
        return view
    }
    
    var isSearchPhotoCalled = false
    func goToSearchPhoto() {
        isSearchPhotoCalled = true
    }
    
    
    
    
}


