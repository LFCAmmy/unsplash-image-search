//
//  Unsplash_Image_SearchTests.swift
//  Unsplash Image SearchTests
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import XCTest
@testable import Unsplash_Image_Search

class Unsplash_Image_SearchTests: XCTestCase {
    
    private var sut: HomeService!
    
    override class func setUp() {
        super.setUp()
    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

}
