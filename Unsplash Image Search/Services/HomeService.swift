//
//  HomeService.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import Foundation

protocol HomeService {
    func fetchCollectionPhotos(page: Int,
                               success: @escaping complitionSuccess<[UnSplashPhoto]>,
                               failed: @escaping complitionFailed)
    
    func searchPhotos(query: String,
                      order_by: String,
                      color: String,
                      orientation: String,
                      page: Int,
                      success: @escaping complitionSuccess<SearchPhoto>,
                      failed: @escaping complitionFailed)
}
extension HomeService {
    
    func fetchCollectionPhotos(page: Int = 1,
                               success: @escaping complitionSuccess<[UnSplashPhoto]>,
                               failed: @escaping complitionFailed) {
        if !NetworkUtility.shared.isNetworkAvailable() {
            failed("")
            return
        }
        
        let url = URL(string: Endpoints.getCollectionPhotos)
        let newURL = url!.appending("page", value: "\(page)")

        BaseService.shared.GET("\(newURL)", params: nil).responseJSON { (responseData) in
            switch responseData.result {
            case .success(_):
                    do {
                        let unSplashPhoto = try JSONDecoder().decode([UnSplashPhoto].self, from: responseData.data!)
                        success(unSplashPhoto)
                    } catch {
                        print(error)
                        failed(error.localizedDescription)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    failed(error.localizedDescription)
            }
        }
    }
    
    func searchPhotos(query: String,
                      order_by: String = "relevant",
                      color: String = "Any",
                      orientation: String = "Any",
                      page: Int = 1,
                      success: @escaping complitionSuccess<SearchPhoto>,
                      failed: @escaping complitionFailed) {
        if !NetworkUtility.shared.isNetworkAvailable() {
            failed("")
            return
        }
        
        let url = URL(string: Endpoints.searchPhotos)
        var newURL = url!.appending("query", value: "\(query)")
                         .appending("page", value: "\(page)")
                         .appending("order_by", value: order_by)
        if color != "Any" {
            newURL = newURL.appending("color", value: color)
        }
        if orientation != "Any" {
            newURL = newURL.appending("orientation", value: orientation)
        }
        
        BaseService.shared.GET("\(newURL)", params: nil).responseJSON { (responseData) in
            switch responseData.result {
            case .success(_):
                    do {
                        let unSplashPhoto = try JSONDecoder().decode(SearchPhoto.self, from: responseData.data!)
                        success(unSplashPhoto)
                    } catch {
                        print(error)
                        failed(error.localizedDescription)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    failed(error.localizedDescription)
            }
        }
    }
}
