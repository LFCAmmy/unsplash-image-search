//
//  PhotoCollectionViewCell.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        photoImageView.set(borderWidth: 1, of: .customBlackColor())
        photoImageView.set(cornerRadius: 8)
    }
    
    func renderData(data: UnSplashPhotoViewModel) {
        photoImageView.kf.setImage(with: data.urls.thumb.absoluteUrl())
        photoImageView.kf.setImage(with: data.urls.regular.absoluteUrl())
        
    }

}
