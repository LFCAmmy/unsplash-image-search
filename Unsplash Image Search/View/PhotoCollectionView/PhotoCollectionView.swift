//
//  Photo.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//

import UIKit

class PhotoCollectionView: UICollectionView {

    fileprivate(set) var items : [UnSplashPhotoViewModel] = [UnSplashPhotoViewModel]()
    private let itemsPerRow: CGFloat = 2
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 20.0, right: 0.0)
    private var isLoading = false
    
    var loadMore: (()-> Void)?
    var isModeDataAvailable = true
    
    var ownerSelf: UIViewController!
   
    func initWith(withItem items: [UnSplashPhotoViewModel]) {
        initDefaults()
        self.items = items
        self.reloadData()
    }
    
    private func initDefaults() {
        register(PhotoCollectionViewCell.self)
        delegate = self
        dataSource = self
        if let layout = self.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
    }
}

extension PhotoCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }

    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : PhotoCollectionViewCell = self.dequeueReusableCell(forIndexPath: indexPath)
        cell.renderData(data: items[indexPath.item])
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
        
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = self.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem - 5, height: widthPerItem - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.imageTapped(image: items[indexPath.row].urls.full)
    }
    
    func imageTapped(image:String){
        let newImageView = UIImageView()
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        newImageView.kf.setImage(with: image.absoluteUrl())
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.ownerSelf.view.addSubview(newImageView)
        self.ownerSelf.navigationController?.isNavigationBarHidden = true
        self.ownerSelf.tabBarController?.tabBar.isHidden = true
    }

    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.ownerSelf.navigationController?.isNavigationBarHidden = false
        sender.view?.removeFromSuperview()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if (offsetY > contentHeight - scrollView.frame.height * 4) && !isLoading && isModeDataAvailable {
            loadMoreData()
        }
    }
    
    func loadMoreData() {
        if !self.isLoading {
            self.isLoading = true
            DispatchQueue.global().async {
                // Fake background loading task for 2 seconds
                sleep(2)
                // Download more data here
                self.loadMore?()
                DispatchQueue.main.async {
                    self.reloadData()
                    self.isLoading = false
                }
            }
        }
    }
}

extension PhotoCollectionView: PinterestLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        let data = items[indexPath.item]
        let aspectRatio : Double =  Double(data.width) / Double(data.height)
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = self.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return widthPerItem * CGFloat(aspectRatio)
    }
}
