//
//  WarningViewController.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 11/09/2021.
//

import UIKit

enum WarningType {
    case warning
    
    var title: String {
        switch self {
        case .warning:
            return AppState.errorMsg!
        }
    }
    
    var description: String {
        switch self {
        case .warning:
            return AppState.errorMsg!
        }
    }
    
    var icon: UIImage {
        switch self {
        case .warning:
            return UIImage(named: "warning_icon")!
        }
    }
    
    var actionColor: UIColor {
        switch self {
        case .warning:
            return .buttonColor()
        }
    }
    
    var actionTitle: String {
        switch self {
        case .warning:
            return "OKAY"
        }
        
    }
    
    var cancelTitle: String {
        switch self {
        default:
            return ""
        }
    }
    
    var isHideCancel: Bool {
        switch self {
        default:
            return true
        }
    }
}

class WarningViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var warningImageView: UIImageView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var warningButton: UIButton!
    @IBOutlet weak var warningTitleLabel: UILabel!
    @IBOutlet weak var warningItemNameLabel: UILabel!
    @IBOutlet weak var warningDescriptionLabel: UILabel!
    @IBOutlet weak var containerViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerViewCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    var warningType: WarningType = .warning
    var warningItem: Any?
    var handleWarningAction: (() -> Void)?
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setViews()
        renderData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let _self = self else { return }
            _self.containerView.transform = CGAffineTransform(scaleX: 1, y: 1)
            _self.view.layoutIfNeeded()
            _self.view.setNeedsLayout()
        }
    }
    
    // MARK: - Private methods
    private func setViews() {
        warningTitleLabel.font = UIFont.dmSansBold(size: 18)
        warningItemNameLabel.font = UIFont.dmSansBold(size: 18)
        warningItemNameLabel.textColor = .customBlackColor()
        warningDescriptionLabel.font = UIFont.dmSansBold(size: AppFontsizes.small)
        warningDescriptionLabel.textColor = .customBlackColor()
        cancelButton.titleLabel?.font = UIFont.dmSansBold(size: 13)
        cancelButton.setTitleColor(.white, for: .normal)
        cancelButton.layer.cornerRadius = 8
        cancelButton.backgroundColor = UIColor(hexString: "E80000")
        warningButton.titleLabel?.font = UIFont.dmSansBold(size: 13)
        warningButton.setTitleColor(.white, for: .normal)
        warningButton.layer.cornerRadius = 8
        
        containerView.transform = CGAffineTransform(scaleX: 0.93, y: 0.93)
        containerView.layer.cornerRadius = 20
        containerView.backgroundColor = .backgroundColor()
        warningTitleLabel.handleSpacingWord(lineHeight: 0.85, alignment: .center)
        warningDescriptionLabel.handleSpacingWord(lineHeight: 0.87, alignment: .center)
        
        containerViewTrailingConstraint.isActive = true
        containerViewLeadingConstraint.isActive = true
        containerViewWidthConstraint.isActive = false
        containerViewCenterConstraint.isActive = false
    }
    
    private func renderData() {
        warningItemNameLabel.isHidden = true
        warningImageView.image = warningType.icon
        warningTitleLabel.text = warningType.title
        warningDescriptionLabel.text = warningType.description
        warningButton.setTitle(warningType.actionTitle.uppercased(), for: .normal)
        warningButton.backgroundColor = warningType.actionColor
        cancelButton.setTitle(warningType.cancelTitle.uppercased(), for: .normal)
        cancelButton.isHidden = warningType.isHideCancel
    }
    
    fileprivate func dismissPage(success: (() -> Void)? = nil) {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let _self = self else { return }
            _self.containerView.transform = CGAffineTransform(scaleX: 0.93, y: 0.93)
            _self.view.layoutIfNeeded()
            _self.view.setNeedsLayout()
        }) { [weak self] (finished) in
            guard let _self = self else { return }
            _self.dismiss(animated: false, completion: {
                success?()
            })
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func cancelButtonAciton(_ sender: UIButton) {
        dismissPage()
    }
    
    @IBAction func warningButtonAction(_ sender: UIButton) {
        dismissPage { [weak self] in
            guard let _self = self else { return }
            _self.handleWarningAction?()
        }
    }
}

// MARK: - UIGestureRecognizerDelegate
extension WarningViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldReceive touch: UITouch) -> Bool {
        if let view = touch.view, view.isDescendant(of: containerView) {
            return false
        }
        
        return true
    }
}
