import UIKit

enum EmptyDataType: String {
    case image
    
    var title: String {
        switch self {
        case .image:
            return "Opps! No images saved!"
        }
    }
    
    var description: String {
        switch self {
        case .image:
            return "Please search another image!"
        }
    }
}
    
class EmptyDataView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet weak var emptyTitleLabel: UILabel!
    @IBOutlet weak var emptyDescriptionLabel: UILabel!
    
    // MARK: - Variables
    var emptyDataType: EmptyDataType = .image
    
    // MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        emptyTitleLabel.font = .dmSansBold(size: AppFontsizes.regular)
        emptyTitleLabel.textColor = .customBlackColor()
        
        emptyDescriptionLabel.font = .dmSansRegular(size: AppFontsizes.small)
        emptyDescriptionLabel.textColor = UIColor.customBlackColor().withAlphaComponent(0.5)
        
        self.bordered(withColor: UIColor.customBlackColor().withAlphaComponent(0.1), width: 1, radius: 8)
    }
    
    func renderData() {
        emptyTitleLabel.text = emptyDataType.title
        emptyDescriptionLabel.text = emptyDataType.description
    }
}

