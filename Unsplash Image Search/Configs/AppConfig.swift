//
//  AppConfig.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import Foundation

struct AppConfig {
    struct ServerHosts {
        static let baseURL = "https://api.unsplash.com"
    }
    
    struct Environment {
        static let hostPath = ServerHosts.baseURL
    }
}
