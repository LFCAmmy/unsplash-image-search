//
//  Constants.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import UIKit

func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

struct AppFontsizes {
    static let regular: CGFloat = 16
    static let regular1: CGFloat = 18
    static let small: CGFloat = 14
    static let verySmall: CGFloat = 11
    static let extraSmall: CGFloat = 9
    static let big: CGFloat = 19
    static let bigPlus: CGFloat = 20
    static let veryBig: CGFloat = 24
    static let size26: CGFloat = 26
}

struct APIKey {
    static let accessKey = "FZpIl7feLseFHwV4DScQqiaVULO54C7GRBiqlmDrxdI"
    
    static let secretKey = "F91OpKAWUx6mSzdNVDKDf8SXgS4HwLzNTqGWMYkoOuE"
}

struct Endpoints {
    static let getCollectionPhotos         = "collections/2423569/photos"
    static let searchPhotos                = "search/photos"
}
