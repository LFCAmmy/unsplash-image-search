//
//  AdvanceSearchViewController.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 01/09/2021.
//
//

import UIKit

class AdvanceSearchViewController: UIViewController {
    
    var query = AppState.searchQuery
    
    // MARK: Properties
    
    var presenter: AdvanceSearchModuleInterface?
    
    // MARK: IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sortByTitleLabel: UILabel!
    @IBOutlet weak var sortByCheckImage: UIImageView!
    @IBOutlet weak var relevanceCheckImage: UIImageView!
    @IBOutlet weak var newestCheckImage: UIImageView!
    @IBOutlet weak var colorTitleLabel: UILabel!
    @IBOutlet weak var anyColorCheckImage: UIImageView!
    @IBOutlet weak var blackAndWhiteCheckImage: UIImageView!
    @IBOutlet weak var orientationTitleLabel: UILabel!
    @IBOutlet weak var anyCheckImage: UIImageView!
    @IBOutlet weak var portraitCheckImage: UIImageView!
    @IBOutlet weak var landscapeCheckImage: UIImageView!
    @IBOutlet weak var squareCheckImage: UIImageView!
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet var sortByCheckImages: [UIImageView]!
    @IBOutlet var colorCheckImages: [UIImageView]!
    @IBOutlet var orientationCheckImages: [UIImageView]!
    @IBOutlet var filterLabels: [UILabel]!
    @IBOutlet var toneColorBtns: [UIButton]!
    
    // MARK: VC's Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: IBActions
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func relevanceBtnClicked(_ sender: Any) {
        // remove image of sort by image
        emptySortByImage()
        
        // add image in sort by relevance
        setCheckImage(imageView: relevanceCheckImage)
        AppState.orderBy = "relevant"
    }
    
    @IBAction func newestBtnClicked(_ sender: Any) {
        // remove image of sort by image
        emptySortByImage()
        
        // add image in sort by newest
        setCheckImage(imageView: newestCheckImage)
        AppState.orderBy = "newest"
    }
    
    @IBAction func anyColorBtnClicked(_ sender: Any) {
        // remove image of sort by color
        emptyColorImage()
        
        // remove boarder from all tone colors
        setEmptyCheckColor()
        
        // add image in sort by any color
        setCheckImage(imageView: anyColorCheckImage)
        AppState.color = "Any"
    }
    
    @IBAction func blackAndWhiteBtnClicked(_ sender: Any) {
        // remove image of sort by color
        emptyColorImage()
        
        // remove boarder from all tone colors
        setEmptyCheckColor()
        
        // add image in sort by black and white
        setCheckImage(imageView: blackAndWhiteCheckImage)
        AppState.color = "black_and_white"
    }
    
    @IBAction func anyBtnClicked(_ sender: Any) {
        // remove image of sort by orientation
        emptyOrientationImage()
        
        // add image in sort by any orientation
        setCheckImage(imageView: anyCheckImage)
        AppState.orientation = "Any"
    }
    
    @IBAction func portraitBtnClicked(_ sender: Any) {
        // remove image of sort by orientation
        emptyOrientationImage()
        
        // add image in sort by portrait
        setCheckImage(imageView: portraitCheckImage)
        AppState.orientation = "portrait"
    }
    
    @IBAction func landscapeBtnClicked(_ sender: Any) {
        // remove image of sort by orientation
        emptyOrientationImage()
        
        // add image in sort by landscape
        setCheckImage(imageView: landscapeCheckImage)
        AppState.orientation = "landscape"
    }
    
    @IBAction func squareBtnClicked(_ sender: Any) {
        // remove image of sort by orientation
        emptyOrientationImage()
        
        // add image in sort by squarish
        setCheckImage(imageView: squareCheckImage)
        AppState.orientation = "squarish"
    }
    
    @IBAction func clearBtnClicked(_ sender: Any) {
        // remove all images and boarder from sort by and tone color
        // respectively
        clearFilter()
        
        // apply default filters
        // save default setting to user defaults
        AppState.orderBy = "relevant"
        AppState.color = "Any"
        AppState.orientation = "Any"
        
        setCheckImage(imageView: relevanceCheckImage)
        setCheckImage(imageView: anyColorCheckImage)
        setCheckImage(imageView: anyCheckImage)
        
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        presenter?.advanceSearch()
    }
    // MARK: Other Functions
    
    private func setup() {
        // all setup should be done here
        view.backgroundColor = .backgroundColor()
        clearBtn.set(cornerRadius: 8)
        submitBtn.set(cornerRadius: 8)
        setupFilter()
        setupToneBtn()
    }
    
    func setupFilter() {
        // reset all filters to default settings
        clearFilter()
        
        // set selected sort by image
        // if relevant is selected, set select image on relevance
        // else set select image to newest
        if AppState.orderBy == "relevant" {
            setCheckImage(imageView: relevanceCheckImage)
        } else {
            setCheckImage(imageView: newestCheckImage)
        }
        
        // set selected sort by color
        // if any is selected, set select color image on any
        // else if black is selected, set select image to black
        // else if white is selected, set select image to white
        // else if yellow is selected, set select image to yellow
        // else if orange is selected, set select image to orange
        // else if red is selected, set select image to red
        // else if purple is selected, set select image to purple
        // else if magenta is selected, set select image to magenta
        // else if teal is selected, set select image to teal
        // else if blue is selected, set select image to blue
        // else if green is selected, set select image to green
        // else set select image to black and white
        if AppState.color == "Any" {
            setCheckImage(imageView: anyColorCheckImage)
        } else if AppState.color == "black" {
            setCheckColorBtn(pos: 0)
        } else if AppState.color == "white" {
            setCheckColorBtn(pos: 1)
        } else if AppState.color == "yellow" {
            setCheckColorBtn(pos: 2)
        } else if AppState.color == "orange" {
            setCheckColorBtn(pos: 3)
        } else if AppState.color == "red" {
            setCheckColorBtn(pos: 4)
        } else if AppState.color == "purple" {
            setCheckColorBtn(pos: 5)
        } else if AppState.color == "magenta" {
            setCheckColorBtn(pos: 6)
        } else if AppState.color == "green" {
            setCheckColorBtn(pos: 7)
        } else if AppState.color == "teal" {
            setCheckColorBtn(pos: 8)
        } else if AppState.color == "blue" {
            setCheckColorBtn(pos: 9)
        } else {
            setCheckImage(imageView: blackAndWhiteCheckImage)
        }
        
        // set selected sort by orientation
        // if any is selected, set select orientation image on any
        // else if portrait is selected, set orientation image to portrait
        // else if landscape is selected, set orientation image to landscape
        // else set orientation image to square
        if AppState.orientation == "Any" {
            setCheckImage(imageView: anyCheckImage)
        } else if AppState.orientation == "portrait" {
            setCheckImage(imageView: portraitCheckImage)
        } else if AppState.orientation == "landscape" {
            setCheckImage(imageView: landscapeCheckImage)
        } else {
            setCheckImage(imageView: squareCheckImage)
        }
    }
    
    func setupToneBtn() {
        // make all toneColor button to rounded
        // add click listener in all toneColor
        for btn in toneColorBtns {
            btn.rounded()
            btn.addTarget(self, action: #selector(self.buttonClicked(_:)), for: .touchUpInside)
        }
    }
    
    @objc func buttonClicked(_ sender: UIButton!) {
        // set index to the toneColorButton index
        let index = toneColorBtns.index(of: sender)
        
        // set toneColorBtn border in respect to index
        // save color to user default in respect to index
        switch index {
        case 0:
            setCheckColorBtn(pos: 0)
            AppState.color = "black"
        case 1:
            setCheckColorBtn(pos: 1)
            AppState.color = "white"
        case 2:
            setCheckColorBtn(pos: 2)
            AppState.color = "yellow"
        case 3:
            setCheckColorBtn(pos: 3)
            AppState.color = "orange"
        case 4:
            setCheckColorBtn(pos: 4)
            AppState.color = "red"
        case 5:
            setCheckColorBtn(pos: 5)
            AppState.color = "purple"
        case 6:
            setCheckColorBtn(pos: 6)
            AppState.color = "magenta"
        case 7:
            setCheckColorBtn(pos: 7)
            AppState.color = "green"
        case 8:
            setCheckColorBtn(pos: 8)
            AppState.color = "teal"
        case 9:
            setCheckColorBtn(pos: 9)
            AppState.color = "blue"
        default:
            break
        }
    }
    
    func clearFilter() {
        emptySortByImage()
        emptyColorImage()
        emptyOrientationImage()
        setEmptyCheckColor()
    }
    
    func emptySortByImage() {
        for image in sortByCheckImages {
            image.image = nil
        }
    }
    
    func emptyColorImage() {
        for image in colorCheckImages {
            image.image = nil
        }
    }
    
    func emptyOrientationImage() {
        for image in orientationCheckImages {
            image.image = nil
        }
    }
    
    func setCheckImage(imageView: UIImageView) {
        imageView.image = UIImage(named: "check")!
    }
    
    func setCheckColorBtn(pos: Int) {
        toneColorBtns[pos].set(borderWidth: 3, of: .gray)
    }
    
    func setEmptyCheckColor() {
        for color in toneColorBtns {
            color.bordered(withColor: .white, width: 0)
        }
    }
}

// MARK: AdvanceSearchViewInterface
// connecting view to presenter
extension AdvanceSearchViewController: AdvanceSearchViewInterface {
    func show(error: String) {
        AppState.errorMsg = error
        showWarningPage(warningType: .warning, handleWarningAction: nil)
    }
    
    func showLoading() {
        showHUD()
    }
    
    func hideLoading() {
        dismissHUD()
    }
    
    func setFilter(query: String) {
        self.query = query
    }
}
