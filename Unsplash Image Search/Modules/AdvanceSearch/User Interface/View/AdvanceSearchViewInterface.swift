//
//  AdvanceSearchViewInterface.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 01/09/2021.
//
//

protocol AdvanceSearchViewInterface: class {
    func showLoading()
    func hideLoading()
    func show(error: String)
}
