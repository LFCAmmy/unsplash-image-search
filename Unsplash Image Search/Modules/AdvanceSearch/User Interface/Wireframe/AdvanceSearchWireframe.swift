//
//  AdvanceSearchWireframe.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 01/09/2021.
//
//

import UIKit

class AdvanceSearchWireframe {
     weak var view: UIViewController!
}

extension AdvanceSearchWireframe: AdvanceSearchWireframeInput {
    
    var storyboardName: String {return "AdvanceSearch"}
    
    func getMainView() -> UIViewController {
        let service = AdvanceSearchService()
        let interactor = AdvanceSearchInteractor(service: service)
        let presenter = AdvanceSearchPresenter()
        let viewController = viewControllerFromStoryboard(of: AdvanceSearchViewController.self)
        
        viewController.presenter = presenter
        interactor.output = presenter
        presenter.interactor = interactor
        presenter.wireframe = self
        presenter.view = viewController
        
        self.view = viewController
        return viewController
    }
    
    func goToSearchPhoto() {
        view.dismiss(animated: true, completion: nil)
    }
}
