//
//  AdvanceSearchPresenter.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 01/09/2021.
//
//

import Foundation

class AdvanceSearchPresenter {
    
	// MARK: Properties
    
    weak var view: AdvanceSearchViewInterface?
    var interactor: AdvanceSearchInteractorInput?
    var wireframe: AdvanceSearchWireframeInput?

    // MARK: Converting entities
}

 // MARK: AdvanceSearch module interface
 // connecting view to presenter

extension AdvanceSearchPresenter: AdvanceSearchModuleInterface {
    func advanceSearch() {
        view?.showLoading()
        interactor?.advanceSearch()
    }
}

// MARK: AdvanceSearch interactor output interface
// connecting interactor to presenter
extension AdvanceSearchPresenter: AdvanceSearchInteractorOutput {
    
    func searchedPhoto() {
        view?.hideLoading()
        wireframe?.goToSearchPhoto()
    }
    
    func obtained(error: String) {
        view?.hideLoading()
        view?.show(error: error)
    }
    
    
}
