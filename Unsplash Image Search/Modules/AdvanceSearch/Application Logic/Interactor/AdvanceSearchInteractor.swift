//
//  AdvanceSearchInteractor.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 01/09/2021.
//
//

import Foundation

class AdvanceSearchInteractor {
    
    var query = String()

	// MARK: Properties
    
    weak var output: AdvanceSearchInteractorOutput?
    private let service: AdvanceSearchServiceType
    
    // MARK: Initialization
    
    init(service: AdvanceSearchServiceType) {
        self.service = service
    }

    // MARK: Converting entities
}

// MARK: AdvanceSearch interactor input interface
// conencting presenter to interactor

extension AdvanceSearchInteractor: AdvanceSearchInteractorInput {
    func advanceSearch() {
        // call api from HomeServer i.e searchPhotos
        // its for filter purpose
        service.searchPhotos(query: AppState.searchQuery!, order_by: AppState.orderBy!, color: AppState.color!, orientation: AppState.orientation!, page: 1) { [weak self] searchedPhoto in
            guard let self = self else { return }
            self.output?.searchedPhoto()
            
            // save data retrived from api response to notification center using name .advanceSearch
            NotificationCenter.default.post(name: .advanceSearch, object: (searchedPhoto))
        } failed: { [weak self] error in
            guard let self = self else { return }
            self.output?.obtained(error: error)
        }
    }
    
}
