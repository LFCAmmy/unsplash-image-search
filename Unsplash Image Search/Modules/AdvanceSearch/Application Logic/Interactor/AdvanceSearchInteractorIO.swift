//
//  AdvanceSearchInteractorIO.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 01/09/2021.
//
//

protocol AdvanceSearchInteractorInput: class {
    func advanceSearch()
}

protocol AdvanceSearchInteractorOutput: class {
    func searchedPhoto()
    func obtained(error: String)
}
