//
//  AdvanceSearchModuleInterface.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 01/09/2021.
//
//

protocol AdvanceSearchModuleInterface: class {
    func advanceSearch()
}
