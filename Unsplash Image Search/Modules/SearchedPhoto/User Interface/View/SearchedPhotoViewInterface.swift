//
//  SearchedPhotoViewInterface.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//
//

protocol SearchedPhotoViewInterface: class {
    func showLoading()
    func hideLoading()
    func show(data: [UnSplashPhotoViewModel], totalPage: Int)
    func show(moreData: [UnSplashPhotoViewModel], totalPage: Int)
    func show(filteredData: [UnSplashPhotoViewModel], totalPage: Int)
    func show(error: String)
}
