//
//  SearchedPhotoViewController.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//
//

import UIKit

class SearchedPhotoViewController: UIViewController {
    
    var photos = [UnSplashPhotoViewModel]()
    var query = String()
    var isLoadedAlready = false
    
    var isMoreDataAvailable = true
    var pageNo = 1
    var totalPage = 0
    
    private var emptyDataView: EmptyDataView!
    // MARK: Properties
    
    var presenter: SearchedPhotoModuleInterface?
    
    // MARK: IBOutlets
    @IBOutlet weak var collectionView: PhotoCollectionView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var emptyContainerView: UIView!
    
    // MARK: VC's Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.title = query
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // assign custom item to navigation bar
        navigationItem.leftBarButtonItem = leftBarButtonItem()
        navigationItem.rightBarButtonItem = rightBarButtonItem()
    }
    
    // MARK: IBActions
    
    // MARK: Other Functions
    
    private func setup() {
        // all setup should be done here
        setupEmptyView()
        setupCollectionView()
        setupView()
        
        // check if the pagination is required or not
        if pageNo <= totalPage {
            self.collectionView.isModeDataAvailable = false
        }
    }
    
    func setupCollectionView() {
        // initialize collection view with photos array
        self.collectionView.initWith(withItem: photos)
        
        // pass current view controller to collection view
        self.collectionView.ownerSelf = self
        
        // to know if the pagination is called through scrolling
        self.collectionView.loadMore = {
            self.presenter?.showMorePhotos(pageNo: self.pageNo)
        }
    }
    
    private func setupEmptyView() {
        // initialize empty view for image
        emptyDataView = createEmptyView(emptyDataType: .image)
        emptyDataView.frame = emptyContainerView.bounds
        emptyContainerView.addSubview(emptyDataView)
        
        // hide colleciton view if the count of photos array is 0
        self.collectionView.isHidden = photos.count == 0
        
        // hide emptyContainerView if the count of photos array greater 0
        self.emptyContainerView.isHidden = photos.count != 0
        
        // render data for emptyContainerView
        self.emptyDataView.renderData()
    }
    
    func setupView() {
        self.view.backgroundColor = .backgroundColor()
        containerView.set(cornerRadius: 16)
    }
    
    func rightBarButtonItem(iconName: String = "right-bar-icon" ) -> UIBarButtonItem{
        let rightBarButtonItem = UIBarButtonItem(image: UIImage(named: iconName)?.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(openAdvanceSerch))
        rightBarButtonItem.tintColor = UIColor.black
        return rightBarButtonItem
    }
    
    func leftBarButtonItem(iconName: String = "back" ) -> UIBarButtonItem{
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: iconName)?.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(backButtonActionClicks))
        leftBarButtonItem.tintColor = UIColor.black
        return leftBarButtonItem
    }
    
    @objc func openAdvanceSerch() {
        self.presenter?.openAdvanceSearch()
    }
    
    @objc private func backButtonActionClicks() {
        presenter?.goToLandingPage()
    }
}

// MARK: SearchedPhotoViewInterface
// connecting presenter to view
extension SearchedPhotoViewController: SearchedPhotoViewInterface {
    func show(data: [UnSplashPhotoViewModel], totalPage: Int) {
        // increase the page number by 1
        pageNo = pageNo + 1
        
        // get query from user defaults i.e AppState.searchQuery
        query = AppState.searchQuery!
        
        // assign response data
        self.photos = data
        
        // assign total page number
        self.totalPage = totalPage
    }
    
    func show(moreData: [UnSplashPhotoViewModel], totalPage: Int) {
        // check if the pagination is required or not
        if pageNo <= totalPage {
            self.collectionView.isModeDataAvailable = false
        }
        
        // increase the page number by 1
        pageNo = pageNo + 1
        
        // get query from user defaults i.e AppState.searchQuery
        query = AppState.searchQuery!
       
        // assign response data along with current data
        self.photos = self.photos + moreData
        
        // initialize collection view
        self.setupCollectionView()
    }
    
    func show(filteredData: [UnSplashPhotoViewModel], totalPage: Int) {
        // check if the pagination is required or not
        if filteredData.count != 10 {
            self.collectionView.isModeDataAvailable = false
        }
        
        // assign pageNo to default number i.e 1
        pageNo = 1
        
        // get query from user defaults i.e AppState.searchQuery
        query = AppState.searchQuery!
        
        // assign response data along with current data
        self.photos = filteredData
        
        // initialize collection view
        self.setupCollectionView()
        
        // initialize empty view
        self.setupEmptyView()
    }
    
    func show(error: String) {
        // assign search text to default valiable AppState.errorMsg
        AppState.errorMsg = error
        
        // open warning view and show the data refering to .warning
        showWarningPage(warningType: .warning, handleWarningAction: nil)
    }
    
    func showLoading() {
        // show progressing bar
        showHUD()
    }
    
    func hideLoading() {
        // stop progressing bar
        dismissHUD()
    }
}
