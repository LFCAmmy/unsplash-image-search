//
//  SearchedPhotoPresenter.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//
//

import Foundation

class SearchedPhotoPresenter {
    
	// MARK: Properties
    
    weak var view: SearchedPhotoViewInterface?
    var interactor: SearchedPhotoInteractorInput?
    var wireframe: SearchedPhotoWireframeInput?

    // MARK: Converting entities
    private func convertInput(models: [UnSplashPhotoStructure]) -> [UnSplashPhotoViewModel] {
        models.map({ model -> UnSplashPhotoViewModel in
            let strucutre = UnSplashPhotoViewModel(id: model.id,
                                                   width: model.width,
                                                   height: model.height,
                                                   color: model.color,
                                                   urls: model.urls)
            return strucutre
        })
    }
}

 // MARK: SearchedPhoto module interface
 // connecting view to presenter

extension SearchedPhotoPresenter: SearchedPhotoModuleInterface {
    func openAdvanceSearch() {
        self.wireframe?.openAdvanceSearch()
    }
    
    func showMorePhotos(pageNo: Int) {
        self.view?.showLoading()
        self.interactor?.showMorePhotos(pageNo: pageNo)
    }
    
    func goToLandingPage() {
        self.wireframe?.openLandingPage()
    }
}

// MARK: SearchedPhoto interactor output interface
// connecting interactor to presenter

extension SearchedPhotoPresenter: SearchedPhotoInteractorOutput {
    func obtained(data: [UnSplashPhotoStructure], totalPage: Int) {
        let viewModel = convertInput(models: data)
        view?.show(data: viewModel, totalPage: totalPage)
    }
    
    func obtained(moreData: [UnSplashPhotoStructure], totalPage: Int) {
        self.view?.hideLoading()
        let viewModel = convertInput(models: moreData)
        view?.show(moreData: viewModel, totalPage: totalPage)
    }
    
    func obtained(filteredData: [UnSplashPhotoStructure], totalPage: Int) {
        let viewModel = convertInput(models: filteredData)
        view?.show(filteredData: viewModel, totalPage: totalPage)
    }
    
    func obtained(error: String) {
        view?.hideLoading()
        view?.show(error: error)
    }
    
}
