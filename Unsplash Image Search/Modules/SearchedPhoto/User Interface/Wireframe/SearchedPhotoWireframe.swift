//
//  SearchedPhotoWireframe.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//
//

import UIKit

class SearchedPhotoWireframe {
     weak var view: UIViewController!
}

extension SearchedPhotoWireframe: SearchedPhotoWireframeInput {
    
    var storyboardName: String {return "SearchedPhoto"}
    
    func getMainView() -> UIViewController {
        let service = SearchedPhotoService()
        let interactor = SearchedPhotoInteractor(service: service)
        let presenter = SearchedPhotoPresenter()
        let viewController = viewControllerFromStoryboard(of: SearchedPhotoViewController.self)
        
        viewController.presenter = presenter
        interactor.output = presenter
        presenter.interactor = interactor
        presenter.wireframe = self
        presenter.view = viewController
        
        self.view = viewController
        return viewController
    }
    
    func openAdvanceSearch() {
        // present AdvanceSearchViewController with AdvanceSearchWireframe
        let vc = AdvanceSearchWireframe().getMainView()
        view.present(vc, animated: true, completion: nil)
    }
    
    func openLandingPage() {
        // check if the view has navigation controller
        // to minimize error
        if let navVC = view.navigationController {
            // go back to previous controller
            navVC.popViewController(animated: true)
        }
    }
}
