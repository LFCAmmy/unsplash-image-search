//
//  SearchedPhotoWireframeInput.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//
//

import Foundation

protocol SearchedPhotoWireframeInput: WireframeInput {
    func openAdvanceSearch()
    func openLandingPage()
}
