//
//  SearchedPhotoInteractorIO.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//
//

protocol SearchedPhotoInteractorInput: class {
    func getSearchPhoto()
    func showMorePhotos(pageNo: Int)
    func getFilteredSearchPhoto()
}

protocol SearchedPhotoInteractorOutput: class {
    func obtained(data: [UnSplashPhotoStructure], totalPage: Int)
    func obtained(moreData: [UnSplashPhotoStructure], totalPage: Int)
    func obtained(filteredData: [UnSplashPhotoStructure], totalPage: Int)
    func obtained(error: String)
}
