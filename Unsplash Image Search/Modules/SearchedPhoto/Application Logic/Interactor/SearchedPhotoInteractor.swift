//
//  SearchedPhotoInteractor.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//
//

import Foundation

class SearchedPhotoInteractor {
    
    var searchedPhoto : SearchPhoto?
    var filteredSearchedPhoto : SearchPhoto?

	// MARK: Properties
    
    weak var output: SearchedPhotoInteractorOutput?
    private let service: SearchedPhotoServiceType
    
    // MARK: Initialization
    
    init(service: SearchedPhotoServiceType) {
        self.service = service
        
        // get data from notification center using key .searchPhoto
        NotificationCenter.default.addObserver(self, selector: #selector(self.getSearchedPhoto(_:)), name: .searchedPhoto, object: nil)
        
        // get data from notification center using key .getFilteredSearchedPhoto
        NotificationCenter.default.addObserver(self, selector: #selector(self.getFilteredSearchedPhoto(_:)), name: .advanceSearch, object: nil)
    }
    
    @objc private func getSearchedPhoto(_ notification: Notification) {
        if let searchedPhoto = notification.object as? (SearchPhoto) {
            self.searchedPhoto = searchedPhoto
            getSearchPhoto()
        }
    }

    @objc private func getFilteredSearchedPhoto(_ notification: Notification) {
        if let searchedPhoto = notification.object as? (SearchPhoto) {
            self.filteredSearchedPhoto = searchedPhoto
            getFilteredSearchPhoto()
        }
    }

    // MARK: Converting entities
    private func convertInput(models: [UnSplashPhoto]) -> [UnSplashPhotoStructure] {
        models.map({ model -> UnSplashPhotoStructure in
            let strucutre = UnSplashPhotoStructure(id: model.id,
                                                   width: model.width,
                                                   height: model.height,
                                                   color: model.color,
                                                   urls: model.urls)
            return strucutre
        })
    }
}

// MARK: SearchedPhoto interactor input interface
// conencting presenter to interactor

extension SearchedPhotoInteractor: SearchedPhotoInteractorInput {
    
    func getSearchPhoto() {
        // convert the model retrieved from api to structure
        let structure = convertInput(models: searchedPhoto!.results)
        output?.obtained(data: structure, totalPage: searchedPhoto!.total_pages)
    }
    
    func getFilteredSearchPhoto() {
        // convert the model retrieved from api to structure
        let structure = convertInput(models: filteredSearchedPhoto!.results)
        output?.obtained(filteredData: structure, totalPage: filteredSearchedPhoto!.total_pages)
    }
    
    func showMorePhotos(pageNo: Int) {
        // call api from HomeServer i.e searchPhotos with page number
        // it is for pagination purpose
        service.searchPhotos(query: AppState.searchQuery!, order_by: AppState.orderBy!, color: AppState.color!, orientation: AppState.orientation!, page: pageNo) { [weak self] searchedPhoto in
            guard let self = self else { return }
            self.searchedPhoto = searchedPhoto
            
            // convert the model retrieved from api to structure
            let structure = self.convertInput(models: searchedPhoto.results)
            self.output?.obtained(moreData: structure, totalPage: searchedPhoto.total_pages)
        } failed: { [weak self] error in
            guard let self = self else { return }
            self.output?.obtained(error: error)
        }

    }
}
