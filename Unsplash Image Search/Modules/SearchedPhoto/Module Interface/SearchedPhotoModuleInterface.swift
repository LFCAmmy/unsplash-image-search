//
//  SearchedPhotoModuleInterface.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//
//

protocol SearchedPhotoModuleInterface: class {
    func openAdvanceSearch()
    func showMorePhotos(pageNo: Int)
    func goToLandingPage()
}
