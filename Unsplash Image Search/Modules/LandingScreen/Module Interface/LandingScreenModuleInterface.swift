//
//  LandingScreenModuleInterface.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//
//

protocol LandingScreenModuleInterface: class {
    func viewIsReady()
    func showMorePhotos(pageNo: Int)
    func searchPhoto() 
}
