//
//  LandingScreenInteractor.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//
//

import Foundation

class LandingScreenInteractor {
    
    var photos = [UnSplashPhoto]()
    private static var cache: URLCache = {
        let memoryCapacity = 50 * 1024 * 1024
        let diskCapacity = 100 * 1024 * 1024
        let diskPath = "unsplash"
        
        if #available(iOS 13.0, *) {
            return URLCache(
                memoryCapacity: memoryCapacity,
                diskCapacity: diskCapacity,
                directory: URL(fileURLWithPath: diskPath, isDirectory: true)
            )
        }
        else {
            #if !targetEnvironment(macCatalyst)
            return URLCache(
                memoryCapacity: memoryCapacity,
                diskCapacity: diskCapacity,
                diskPath: diskPath
            )
            #else
            fatalError()
            #endif
        }
    }()
	// MARK: Properties
    
    var output: LandingScreenInteractorOutput?
    private let service: LandingScreenServiceType
    
    // MARK: Initialization
    
    init(service: LandingScreenServiceType) {
        self.service = service
    }
    
    // MARK: Converting entities
    private func convertInput(models: [UnSplashPhoto]) -> [UnSplashPhotoStructure] {
        models.map({ model -> UnSplashPhotoStructure in
            let strucutre = UnSplashPhotoStructure(id: model.id,
                                                   width: model.width,
                                                   height: model.height,
                                                   color: model.color,
                                                   urls: model.urls)
            return strucutre
        })
    }
}

// MARK: LandingScreen interactor input interface
// conencting presenter to interactor
extension LandingScreenInteractor: LandingScreenInteractorInput {

    func getCategoryPhotos() {
        let url = URL(string: Endpoints.getCollectionPhotos)!
        let newURL = url.appending("page", value: "1")
        if let cachedResponse = LandingScreenInteractor.cache.cachedResponse(for: URLRequest(url: newURL)) {
            print("cachedResponse.data")
            print(cachedResponse.data)
            return
        }

        // call api from HomeServer i.e fetchCollectionPhotos
        service.fetchCollectionPhotos { [weak self] unSplashPhotos in
            guard let self = self else { return }
            self.photos = unSplashPhotos
            
            // convert the model retrieved from api to structure
            let structure = self.convertInput(models: unSplashPhotos)
            self.output?.obtained(model: structure)
        } failed: { [weak self] error in
            guard let self = self else { return }
            self.output?.obtained(error: error)
        }
    }
    
    func getMorePhotos(pageNo: Int) {
        // call api from HomeServer i.e fetchCollectionPhotos with page number
        // it is for pagination purpose
        service.fetchCollectionPhotos(page: pageNo) { [weak self] unSplashPhotos in
            guard let self = self else { return }
            self.photos = self.photos + unSplashPhotos
            
            // convert the model retrieved from api to structure
            let structure = self.convertInput(models: unSplashPhotos)
            self.output?.obtained(paginationModel: structure)
        } failed: { [weak self] error in
            guard let self = self else { return }
            self.output?.obtained(error: error)
        }
    }
    
    func getSearchedPhotos() {
        // get search query form user defaults i.e AppState.searchQuery!
        let query = AppState.searchQuery!
        
        // save search options to user defaults
        AppState.orderBy = "relevant"
        AppState.color = "Any"
        AppState.orientation = "Any"
        
        // check if the search query is nil or not
        // if yes show error
        // else call search api
        if query != "" {
            // call api from HomeServer i.e searchPhotos
            service.searchPhotos(query: query) { [weak self] searchedPhoto in
                guard let self = self else { return }
                self.output?.searchedPhoto()
                
                // save data retrived from api response to notification center using name .searchedPhoto
                NotificationCenter.default.post(name: .searchedPhoto, object: (searchedPhoto))
            } failed: { [weak self] error in
                guard let self = self else { return }
                self.output?.obtained(error: error)
            }
        } else {
            // return error message Search text cannot be empty
            self.output?.obtained(error: "Search text cannot be empty")
        }
    }
    
}
