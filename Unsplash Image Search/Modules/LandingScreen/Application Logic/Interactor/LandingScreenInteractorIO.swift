//
//  LandingScreenInteractorIO.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//
//

protocol LandingScreenInteractorInput: class {
    func getCategoryPhotos()
    func getMorePhotos(pageNo: Int)
    func getSearchedPhotos() 
}

protocol LandingScreenInteractorOutput: class {
    func obtained(model: [UnSplashPhotoStructure])
    func obtained(paginationModel: [UnSplashPhotoStructure])
    func searchedPhoto()
    func obtained(error: String)
}
