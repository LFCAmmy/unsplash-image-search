//
//  LandingScreenPresenter.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//
//

import Foundation

class LandingScreenPresenter {
    
	// MARK: Properties
    
    weak var view: LandingScreenViewInterface?
    var interactor: LandingScreenInteractorInput?
    var wireframe: LandingScreenWireframeInput?

    // MARK: Converting entities
    private func convertInput(models: [UnSplashPhotoStructure]) -> [UnSplashPhotoViewModel] {
        models.map({ model -> UnSplashPhotoViewModel in
            let strucutre = UnSplashPhotoViewModel(id: model.id,
                                                   width: model.width,
                                                   height: model.height,
                                                   color: model.color,
                                                   urls: model.urls)
            return strucutre
        })
    }
}

 // MARK: LandingScreen module interface
 // connecting view to presenter

extension LandingScreenPresenter: LandingScreenModuleInterface {
    
    func viewIsReady() {
        view?.showLoading()
        self.interactor?.getCategoryPhotos()
    }
    
    func showMorePhotos(pageNo: Int) {
        view?.showLoading()
        self.interactor?.getMorePhotos(pageNo: pageNo)
    }
    
    func searchPhoto() {
        view?.showLoading()
        self.interactor?.getSearchedPhotos() 
    }
}

// MARK: LandingScreen interactor output interface
// connecting interactor to presenter

extension LandingScreenPresenter: LandingScreenInteractorOutput {
    func searchedPhoto() {
        view?.hideLoading()
        wireframe?.goToSearchPhoto()
    }
    
    func obtained(model: [UnSplashPhotoStructure]) {
        view?.hideLoading()
        let viewModel = self.convertInput(models: model)
        view?.show(data: viewModel)
    }
    
    func obtained(paginationModel: [UnSplashPhotoStructure]) {
        view?.hideLoading()
        let viewModel = self.convertInput(models: paginationModel)
        view?.show(paginationData: viewModel)
    }
    
    func obtained(error: String) {
        view?.hideLoading()
        view?.show(error: error)
    }
    
}
