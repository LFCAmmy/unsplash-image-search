//
//  LandingScreenWireframeInput.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//
//

import Foundation

protocol LandingScreenWireframeInput: WireframeInput {
    func goToSearchPhoto()
}
