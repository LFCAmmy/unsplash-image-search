//
//  LandingScreenWireframe.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//
//

import UIKit

class LandingScreenWireframe {
    weak var view: UIViewController!
    private lazy var searchedPhotoWireframe: SearchedPhotoWireframe = {SearchedPhotoWireframe()}()
}

extension LandingScreenWireframe: LandingScreenWireframeInput {
    
    var storyboardName: String {return "LandingScreen"}
    
    func getMainView() -> UIViewController {
        let service = LandingScreenService()
        let interactor = LandingScreenInteractor(service: service)
        let presenter = LandingScreenPresenter()
        let viewController = viewControllerFromStoryboard(of: LandingScreenViewController.self)
        
        viewController.presenter = presenter
        interactor.output = presenter
        presenter.interactor = interactor
        presenter.wireframe = self
        presenter.view = viewController
        
        self.view = viewController
        return viewController
    }
    
    func goToSearchPhoto() {
        // check if the view has navigation controller
        // to minimize error
        if let navVC = view.navigationController {
            navVC.pushViewController(searchedPhotoWireframe.getMainView(), animated: true)
        }
    }
}
