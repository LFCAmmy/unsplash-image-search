//
//  LandingScreenViewInterface.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//
//

protocol LandingScreenViewInterface: class {
    func showLoading()
    func hideLoading()
    func show(data: [UnSplashPhotoViewModel])
    func show(paginationData: [UnSplashPhotoViewModel])
    func show(error: String)
}
