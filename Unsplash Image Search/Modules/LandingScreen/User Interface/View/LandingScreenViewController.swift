//
//  LandingScreenViewController.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//
//

import UIKit
import UnsplashPhotoPicker

class LandingScreenViewController: UIViewController {
    
    private let itemsPerRow: CGFloat = 2
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 20.0, right: 0.0)
    
    private var photos = [UnSplashPhotoViewModel]()
    
    var isMoreDataAvailable = true
    var pageNo = 1
    
    // MARK: Properties
    
    var presenter: LandingScreenModuleInterface?
    
    // MARK: IBOutlets
    @IBOutlet weak var collectionView: PhotoCollectionView!
    @IBOutlet weak var searchQueryTextField: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    // MARK: VC's Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.title = "Unsplash"
        self.presenter?.viewIsReady()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // for large title navigation bar
        handleLargeTitle()
    }
    
    // MARK: IBActions
    
    @IBAction func searchBtnClicked(_ sender: Any) {
        // assign search text to default valiable AppState.searchQuery
        AppState.searchQuery = searchQueryTextField.text ?? ""
        // call searchPhoto method of presenter
        presenter?.searchPhoto() 
    }
    
    // MARK: Other Functions
    
    private func setup() {
        // all setup should be done here
        setupCollectionView()
        setupView()
    }
    
    func setupView() {
        view.backgroundColor = .backgroundColor()
        containerView.backgroundColor = .white
        containerView.set(cornerRadius: 16)
    }
    
    func setupCollectionView() {
        // initialize collection view with photos array
        self.collectionView.initWith(withItem: photos)
        
        // pass current view controller to collection view
        self.collectionView.ownerSelf = self
        
        // to know if the pagination is called through scrolling
        self.collectionView.loadMore = {
            self.presenter?.showMorePhotos(pageNo: self.pageNo)
        }
        
        self.collectionView.reloadData()
    }
}

// MARK: LandingScreenViewInterface
// connecting presenter to view
extension LandingScreenViewController: LandingScreenViewInterface {
    func show(data: [UnSplashPhotoViewModel]) {
        // check if the pagination is required or not
        if data.count != 10 {
            self.collectionView.isModeDataAvailable = false
        }
        
        // increase the page number by 1
        pageNo = pageNo + 1
        
        // assign response data
        self.photos = data
        
        // initialize collection view
        setupCollectionView()
    }
    
    func show(paginationData: [UnSplashPhotoViewModel]) {
        // check if the pagination is required or not
        if paginationData.count != 10 {
            self.collectionView.isModeDataAvailable = false
        }
        
        // increase the page number by 1
        pageNo = pageNo + 1
        
        // assign response data along with current data
        self.photos = self.photos + paginationData
        
        // initialize collection view
        setupCollectionView()
    }
    
    func show(error: String) {
        // assign search text to default valiable AppState.errorMsg
        AppState.errorMsg = error
        
        // open warning view and show the data refering to .warning
        showWarningPage(warningType: .warning, handleWarningAction: nil)
    }
    
    func showLoading() {
        // show progressing bar
        showHUD()
    }
    
    func hideLoading() {
        // stop progressing bar
        dismissHUD()
    }
    
}
