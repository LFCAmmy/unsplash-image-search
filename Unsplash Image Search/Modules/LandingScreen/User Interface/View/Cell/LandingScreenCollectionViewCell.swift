//
//  LandingScreenCollectionViewCell.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import UIKit
import Kingfisher

class LandingScreenCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var photoImageView: UIImageView!

    func renderData(data: UnSplashPhotoViewModel) {
        // render thumb image first in image using Kingfisher library
        photoImageView.kf.setImage(with: data.urls.thumb.absoluteUrl())
        // render regular image in image using Kingfisher library
        photoImageView.kf.setImage(with: data.urls.regular.absoluteUrl())
        
        photoImageView.set(borderWidth: 1, of: .customBlackColor())
        contentView.backgroundColor = .backgroundColor()
        photoImageView.set(cornerRadius: 8)
    }
}
