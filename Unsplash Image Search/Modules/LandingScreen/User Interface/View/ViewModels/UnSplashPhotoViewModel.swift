//
//  UnSplashPhotoViewModel.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import Foundation

struct UnSplashPhotoViewModel: Codable {
    var id : String
    var width: Int
    var height: Int
    var color: String
    var urls : URLKind
}
