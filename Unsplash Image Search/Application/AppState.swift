//
//  AppState.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 06/09/2021.
//

import Foundation

struct AppState {
    
    static let saveCurrentUserKey = "AppState.saveCurrentUserKey"
    
    static var searchQuery: String? {
        get {
            return UserDefaults.standard.string(forKey: "searchQuery")
        }
        set {
            UserDefaults.standard.setValue(newValue!, forKey: "searchQuery")
        }
    }
    
    static var orderBy: String? {
        get {
            return UserDefaults.standard.string(forKey: "orderBy")
        }
        set {
            UserDefaults.standard.setValue(newValue!, forKey: "orderBy")
        }
    }
    
    static var color: String? {
        get {
            return UserDefaults.standard.string(forKey: "color")
        }
        set {
            UserDefaults.standard.setValue(newValue!, forKey: "color")
        }
    }
    
    static var orientation: String? {
        get {
            return UserDefaults.standard.string(forKey: "orientation")
        }
        set {
            UserDefaults.standard.setValue(newValue!, forKey: "orientation")
        }
    }
    
    static var errorMsg: String? {
        get {
            return UserDefaults.standard.string(forKey: "errorMsg")
        }
        set {
            UserDefaults.standard.setValue(newValue!, forKey: "errorMsg")
        }
    }
}
