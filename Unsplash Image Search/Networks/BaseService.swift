//
//  BaseService.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import Foundation
import Alamofire

typealias complitionSuccess<T> = (T) -> ()
typealias complicationEmptySuccess = () -> ()
typealias complitionFailed = complitionSuccess<String>
typealias complitionFailedwithModel<T> = (T) -> ()
typealias complitionImageFailed = complitionSuccess<(String, String)>

class BaseService {
    
    static let shared = BaseService()
    
    // MARK: - Request methods
    
    func GET(_ endpoint:String, params: [String : Any]?, isEndPointAddress: Bool = false, isDontHaveHeaders: Bool = false) -> Alamofire.DataRequest {
        return sendRequest(.get, endpoint:endpoint, params: params, isEndPointAddress: isEndPointAddress, isDontHaveHeaders: isDontHaveHeaders)
    }
    
    func POST(_ endpoint:String, params: [String : Any]?) -> Alamofire.DataRequest {
        return sendRequest(.post, endpoint:endpoint, params: params)
    }
    
    func PUT(_ endpoint:String, params: [String : Any]?) -> Alamofire.DataRequest {
        return sendRequest(.put, endpoint:endpoint, params: params)
    }
    
    func DELETE(_ endpoint:String, params: [String : Any]?) -> Alamofire.DataRequest {
        return sendRequest(.delete, endpoint:endpoint, params: params)
    }
    
    func PATCH(_ endpoint:String, params: [String : Any]?) -> Alamofire.DataRequest {
        return sendRequest(.patch, endpoint:endpoint, params: params)
    }
    
    // MARK: - Private
    fileprivate func sendRequest(_ method: Alamofire.HTTPMethod,
                                 endpoint: String!,
                                 params:[String : Any]?,
                                 isEndPointAddress: Bool = false, isDontHaveHeaders: Bool = false) -> Alamofire.DataRequest {
        // Configure Alamofire shared manager header
            
        return AF.request(isEndPointAddress ? endpoint : path(endpoint), method: method, parameters: nil, encoding: URLEncoding.default, headers: nil ).validate().responseJSON { (response) in
        // Do nothing.
            print("\(self.path(endpoint))")
            print(response.description)
        }
        
    }
    
    func path(_ endpoint: String) -> String {
        let url = URL(string: "\(AppConfig.Environment.hostPath)/\(endpoint)")
        let newURL = url!.appending("client_id", value: APIKey.accessKey)
        return  "\(newURL)"
    }
}

