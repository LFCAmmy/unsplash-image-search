//
//  NetworkUtility.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import Reachability

class NetworkUtility {
    
    static let shared = NetworkUtility()
    let reachability = try! Reachability()
    
    private init() {}
    
    deinit {
        reachability.stopNotifier()
    }
    
    func isNetworkAvailable() -> Bool {
        return reachability.connection != .unavailable
    }
    
    func checkNetwork(whenUnreachable: (() -> Void)?, whenReachable: (() -> Void)?) {
        if reachability.connection != .unavailable {
            whenReachable?()
        }
        else {
            whenUnreachable?()
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
}
