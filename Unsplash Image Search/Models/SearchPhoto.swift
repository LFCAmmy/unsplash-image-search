//
//  SearchPhoto.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//

import Foundation

struct SearchPhoto: Codable {
    var total: Int
    var total_pages: Int
    var results: [UnSplashPhoto]
}
