//
//  UnSplashPhoto.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import Foundation

struct UnSplashPhoto: Codable {
    
    var id : String
    var width: Int
    var height: Int
    var color: String
    var urls : URLKind
}

struct URLKind: Codable {
    var raw : String
    var full : String
    var regular : String
    var small : String
    var thumb : String
}

