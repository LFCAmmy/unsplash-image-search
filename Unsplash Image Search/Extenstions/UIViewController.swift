//
//  UIViewController.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import UIKit
import SVProgressHUD

extension UIViewController {
    
    func showHUD() {
        Thread.runInMain { [unowned self] in
            SVProgressHUD.show()
            self.showNetworkIndicator()
        }
    }
    
    func dismissHUD(_ delay: TimeInterval? = nil) {
        Thread.runInMain { [unowned self] in
            if let delay = delay {
                SVProgressHUD.dismiss(withDelay: delay)
            }
            else {
                SVProgressHUD.dismiss()
            }
            self.hideNetworkIndicator()
        }
    }
    
    func showNetworkIndicator() {
        if !UIApplication.shared.isNetworkActivityIndicatorVisible {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
    
    func hideNetworkIndicator() {
        if UIApplication.shared.isNetworkActivityIndicatorVisible {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func handleLargeTitle(_ isShow: Bool = true, largeTitleColor: UIColor = UIColor.customBlackColor()) {
        if !isShow {
            navigationController?.navigationItem.largeTitleDisplayMode = .never
            navigationController?.navigationBar.prefersLargeTitles = false
            return
        }
        
        if #available(iOS 13, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithTransparentBackground()
            
            let style = NSMutableParagraphStyle()
            style.firstLineHeadIndent = 13
            let largeTitleAttributes = [NSAttributedString.Key.font: UIFont.dmSansBold(size: 32),
                                        NSAttributedString.Key.foregroundColor: largeTitleColor,
                                        NSAttributedString.Key.paragraphStyle: style]
            navBarAppearance.largeTitleTextAttributes = largeTitleAttributes
            
            let titleAttributes = [NSAttributedString.Key.font: UIFont.dmSansBold(size: 22),
                                   NSAttributedString.Key.foregroundColor: largeTitleColor]
            navBarAppearance.titleTextAttributes = titleAttributes
            
            navigationController?.navigationBar.standardAppearance = navBarAppearance
            navigationController?.navigationBar.compactAppearance = navBarAppearance
            navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
            navigationController?.navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.shadowImage = UIImage()
        }
        else {
            navigationController?.navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
            let style = NSMutableParagraphStyle()
            style.firstLineHeadIndent = 13
            let attributes = [NSAttributedString.Key.font: UIFont.dmSansBold(size: 32),
                              NSAttributedString.Key.foregroundColor: largeTitleColor,
                              NSAttributedString.Key.paragraphStyle: style]
            navigationController?.navigationBar.largeTitleTextAttributes = attributes
        }
    }
    
    func createEmptyView(emptyDataType: EmptyDataType) -> EmptyDataView {
        let emptyDataView: EmptyDataView = EmptyDataView.fromNib()
        emptyDataView.emptyDataType = emptyDataType
        emptyDataView.renderData()
        return emptyDataView
    }
    
    // MARK: - Show waring page
    func showWarningPage(warningType: WarningType, warningItem: Any? = nil, handleWarningAction: (() -> Void)?) {
        let storyboard = UIStoryboard(name: "Warning", bundle: nil)
        let warningVC = storyboard.instantiateViewController(withIdentifier: "WarningViewController") as! WarningViewController
        warningVC.modalPresentationStyle = .overCurrentContext
        warningVC.warningType = warningType
        warningVC.warningItem = warningItem
        // Handle warning action
        warningVC.handleWarningAction = {
            handleWarningAction?()
        }
        present(warningVC, animated: false, completion: nil)
    }
}
