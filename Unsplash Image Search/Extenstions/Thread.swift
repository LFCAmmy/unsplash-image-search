//
//  Thread.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import Foundation

extension Thread {
    static func runInMain(execute: @escaping () -> ()) {
        if !Thread.isMainThread {
            DispatchQueue.main.async {
                execute()
            }
        } else {
            execute()
        }
    }
}
