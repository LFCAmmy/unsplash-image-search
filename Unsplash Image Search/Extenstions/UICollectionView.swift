//
//  UICollectionView.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//

import UIKit

protocol Nameable: class {
    static var defaultName: String { get }
}

extension Nameable {
    static var defaultName: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}

extension UICollectionViewCell : Nameable {
}

// MARK: - Registering cells
extension UICollectionView {
    
    enum TypeOfCell {
        case regular
        case header
        case footer
    }
    
    func register<T: UICollectionViewCell>(_: T.Type, cellType: TypeOfCell = .regular) {
        switch cellType {
        case .regular:
            register(UINib(nibName: T.defaultName, bundle: nil), forCellWithReuseIdentifier: T.defaultName)
        case .header:
            register(UINib(nibName: T.defaultName, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: T.defaultName)
        case .footer:
            register(UINib(nibName: T.defaultName, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: T.defaultName)
        }
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath, cellType: TypeOfCell = .regular) -> T {
        switch cellType {
        case .header:
            guard let cell = dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: T.defaultName, for: indexPath) as? T else {
                fatalError("Could not dequeue cell with identifier: \(T.defaultName)")
            }
            
            return cell
        case .regular:
            guard let cell = dequeueReusableCell(withReuseIdentifier: T.defaultName, for: indexPath) as? T else {
                fatalError("Could not dequeue cell with identifier: \(T.defaultName)")
            }
            
            return cell
        case .footer:
            guard let cell = dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: T.defaultName, for: indexPath) as? T else {
                fatalError("Could not dequeue cell with identifier: \(T.defaultName)")
            }
            return cell
        }
    }
}
