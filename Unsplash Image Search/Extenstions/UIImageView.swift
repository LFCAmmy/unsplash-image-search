//
//  UIImageView.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 29/08/2021.
//

import UIKit

extension UIImageView {
    func getImageWithUrl(url: URL?, placeholder: UIImage = UIImage(), completion:((UIImage) -> Void)?) {
        self.kf.setImage(with: url, placeholder: placeholder) { (result) in
            switch result {
            case .failure(_):
                self.getImageWithUrl(url: url, placeholder: placeholder, completion: completion)
            case .success(let value):
                completion?(value.image)
            }
        }
    }
}
