//
//  UILabel.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 11/09/2021.
//

import UIKit

extension UILabel {
    func handleSpacingWord(text: String? = nil, lineHeight: CGFloat = 0.8, alignment: NSTextAlignment = .left) {
        let attributedString = NSMutableAttributedString(string: text == nil ? self.text ?? "" : text!)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeight
        if alignment == .center {
            paragraphStyle.alignment = .center
        }
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        self.attributedText = attributedString
    }
}
