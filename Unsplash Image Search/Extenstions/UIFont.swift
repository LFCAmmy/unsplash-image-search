//
//  UIFont.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import UIKit

extension UIFont {
    
    class func dmSansBold (size s: CGFloat) -> UIFont {
        return UIFont(name: "DMSans-Bold", size: s)!
    }
    
    class func dmSansRegular (size s: CGFloat) -> UIFont {
        return UIFont(name: "DMSans-Regular", size: s)!
    }
    
    class func dmSansMedium(size s: CGFloat) -> UIFont{
        return UIFont(name: "DMSans-Medium", size: s)!
    }
    
    class func dmSansItalic (size s: CGFloat) -> UIFont {
        return UIFont(name: "DMSans-Italic", size: s)!
    }
}

