//
//  String.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import Foundation

extension String {
    func absoluteUrl() -> URL? {
        return URL(string: self)
    }
}
