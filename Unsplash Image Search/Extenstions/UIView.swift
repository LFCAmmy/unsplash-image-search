//
//  UIView.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 28/08/2021.
//

import UIKit

extension UIView {
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: self, options: nil)![0] as! T
    }
    
    func set(cornerRadius radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func set(border: UIColor) {
        self.layer.borderColor = border.cgColor;
    }
    
    func set(borderWidth: CGFloat) {
        self.layer.borderWidth = borderWidth
    }
    
    func set(borderWidth width: CGFloat, of color: UIColor) {
        self.set(border: color)
        self.set(borderWidth: width)
    }
    
    func rounded() {
        self.set(cornerRadius: self.frame.height/2)
    }
    
    func bordered(withColor color: UIColor, width: CGFloat, radius: CGFloat? = nil) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
        if let radius = radius {
            self.layer.cornerRadius = radius
            self.layer.masksToBounds = true
        }
    }
    
    func addEmptyView(emptyDataType: EmptyDataType) {
        let emptyDataView: EmptyDataView = EmptyDataView.fromNib()
        emptyDataView.frame = self.bounds
        emptyDataView.emptyDataType = emptyDataType
        emptyDataView.renderData()
        emptyDataView.tag = 100
        self.addSubview(emptyDataView)
    }
}
