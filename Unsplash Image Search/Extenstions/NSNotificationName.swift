//
//  NSNotificationName.swift
//  Unsplash Image Search
//
//  Created by Armaan Shrestha on 31/08/2021.
//

import Foundation

extension NSNotification.Name {
    static let searchedPhoto = NSNotification.Name("SearchedPhoto")
    static let advanceSearch = NSNotification.Name("AdvanceSearch")
}
